import { UserDTO } from './../../shared/models/user.dto';
import { CONFIG } from './../../config/config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    constructor(
        private readonly httpClient: HttpClient,
    ) { }

    public getAllUsers(userId: number, perPage: number): Observable<UserDTO[]> {
        return this.httpClient.get<UserDTO[]>(`${CONFIG.SERVER_ADDR}/users?since=${userId}&per_page=${perPage}`);
    }

    public searchUsers(searchText: string): Observable<any> {
        return this.httpClient.get<any>(`${CONFIG.SERVER_ADDR}/search/users?q=${searchText}`);
    }
}

