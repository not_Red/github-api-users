import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificatorService {

  constructor(
    private readonly toastrService: ToastrService,
    ) { }

    public error(message: string) {
      this.toastrService.error(message);
    }
}
