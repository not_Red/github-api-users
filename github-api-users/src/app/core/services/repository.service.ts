import { CONFIG } from './../../config/config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RepositoryDTO } from 'src/app/shared/models/repository.dto';

@Injectable({
    providedIn: 'root'
})
export class RepositoryService {

    constructor(
        private readonly httpClient: HttpClient,

    ) {}

    public getAllRepos(repoId: number, perPage: number): Observable<RepositoryDTO[]> {
        return this.httpClient.get<RepositoryDTO[]>(`${CONFIG.SERVER_ADDR}/repositories?since${repoId}&per_page=${perPage}`);
    }

    public searchRepos(searchText: string): Observable<any> {
        return this.httpClient.get<any>(`${CONFIG.SERVER_ADDR}/search/repositories?q=${searchText}`);
    }
}

