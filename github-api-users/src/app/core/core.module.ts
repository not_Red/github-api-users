import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NotificatorService } from './services/notification.service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-left',
      preventDuplicates: true,
      countDuplicates: true,
    }),
    ],
  providers: [
    NotificatorService
  ],
  exports: [
    HttpClientModule,
  ],
})
export class CoreModule { }
