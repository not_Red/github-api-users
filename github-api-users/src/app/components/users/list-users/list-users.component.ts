import { UserDTO } from './../../../shared/models/user.dto';
import { Component, HostListener, OnInit } from '@angular/core';
import { UsersService } from 'src/app/core/services/users.service';


@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  userId = 0;
  pageSize = 8;
  dataUsers: UserDTO[];
  scrollDebounce = true;
  constructor(
    private readonly usersService: UsersService
  ) {}

  ngOnInit() {
    this.usersService.getAllUsers(this.userId, this.pageSize).subscribe((data) => {
      this.dataUsers = data;
    });
  }

  userProfile(user: UserDTO): void {
    window.open(`${user.html_url}`, '_blank');
  }

  loadMore() {
    const lastUser = this.dataUsers[this.dataUsers.length - 1].id;
    this.usersService.getAllUsers(lastUser, this.pageSize).subscribe((data) => {
      this.dataUsers = this.dataUsers.concat(data);
    });
  }


  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    if (this.scrollDebounce) {
      this.scrollDebounce = false;
      const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
      const max = document.documentElement.scrollHeight;
      const fixedPos = pos + 10;
      if (fixedPos >= max) {
        this.loadMore();
      }
      setTimeout(() => { this.scrollDebounce = true; }, 500);
    }
  }

  handleSearch(r) {
    if (r === '') {
      this.usersService.getAllUsers(this.userId, this.pageSize).subscribe((data) => {
        this.dataUsers = data;
      });
    } else {
      this.usersService.searchUsers(r).subscribe((data) => {
        this.dataUsers = data.items;
      });
    }
  }
}
