import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    ListUsersComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [],
  exports: [
    ListUsersComponent,
  ]
})
export class UsersModule { }
