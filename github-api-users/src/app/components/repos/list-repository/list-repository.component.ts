import { Component, OnInit} from '@angular/core';
import { RepositoryService } from '../../../core/services/repository.service';
import { RepositoryDTO } from '../../../shared/models/repository.dto';

@Component({
  selector: 'app-list-repository',
  templateUrl: './list-repository.component.html',
  styleUrls: ['./list-repository.component.css']
})

export class ListRepositoriesComponent implements OnInit {
  repoId = 0;
  pageSize = 12;
  dataRepositories: RepositoryDTO[];
  constructor(
    private readonly repositoryService: RepositoryService
  ) { }

  ngOnInit() {
    this.repositoryService.getAllRepos(this.repoId, this.pageSize).subscribe((data) => {
      this.dataRepositories = data;
    });
  }

  repositoryPage(repository: RepositoryDTO): void {
    window.open(`${repository.html_url}`, '_blank');
  }

  handleSearch(r: string) {
    if (r === '') {
      this.repositoryService.getAllRepos(this.repoId, this.pageSize).subscribe((data) => {
        this.dataRepositories = data;
      });
    } else {
      this.repositoryService.searchRepos(r).subscribe((data) => {
        this.dataRepositories = data.items;
      });
    }
  }
}
