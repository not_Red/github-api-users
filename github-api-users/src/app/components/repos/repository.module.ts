import { ListRepositoriesComponent } from './list-repository/list-repository.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    ListRepositoriesComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [],
  exports: [
    ListRepositoriesComponent
  ]
})
export class RepositoryModule { }
