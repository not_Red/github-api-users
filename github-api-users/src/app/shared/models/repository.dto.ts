// tslint:disable: variable-name

export class RepositoryDTO {
    public id: number;
    public name: string;
    public html_url: string;
    public description: string;
    public owner: {
        login: string
    };
}
