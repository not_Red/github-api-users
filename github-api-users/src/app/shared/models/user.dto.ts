// tslint:disable: variable-name

export class UserDTO {
    public login: string;
    public id: number;
    public avatar_url: string;
    public html_url: string;
}
