import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<string>();

  @ViewChild('searchInput', { read: ElementRef })
  private searchInput: ElementRef;

  constructor(
  ) { }

  ngOnInit() {

  }

  search() {
      const searchTerm = this.searchInput.nativeElement.value;
      this.searchEvent.emit(searchTerm);
  }
}
