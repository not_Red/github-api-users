import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificatorService } from '../../core/services/notification.service';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
    public constructor(
        private readonly notificator: NotificatorService,
    ) { }

    public intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: any) => {
                if (error.status === 403) {
                    this.notificator.error('403 Forbidden response from Github');
                } else if (error.status >= 400 && error.status !== 403) {
                    this.notificator.error('Oops.. something went wrong.. :(');
                }

                return throwError(error);
            })
        );
    }
}
